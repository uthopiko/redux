import React, { Component, PropTypes } from 'react';
import classnames from 'classnames';
import styles from './FriendListItem.css';

export default class FriendListItem extends Component {
  
  constructor(props, context) {
    super(props, context);

    this.state = {
      value: this.props.name
    };
    console.log(this.state);
  }

  static propTypes = {
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    starred: PropTypes.bool,
    editable: PropTypes.bool,
    starFriend: PropTypes.func.isRequired,
    deleteFriend: PropTypes.func.isRequired
  }

  handleChange = function(event) {
    console.log( event.target.value);
    this.setState({value: event.target.value});
    console.log(this.state.value);
  }

  render () {
    return (
      <li className={styles.friendListItem}>
        <div className={styles.friendInfos}>
          <div>{this.props.editable ? 
            <input type="text" value={this.props.name} onChange={() => this.props.saveName(this.props.id, event.target.value)} /> : 
            <span>{this.props.name}</span>}
          </div>
          <div><small>xx friends in common</small></div>
        </div>
        <div className={styles.friendActions}>
          <button className={`btn btn-default ${styles.btnAction}`} onClick={() => this.props.editFriend(this.props.id)}>
            <i className="fa fa-edit" />
          </button>
          <button className={`btn btn-default ${styles.btnAction}`} onClick={() => this.props.starFriend(this.props.id)}>
            <i className={classnames('fa', { 'fa-star': this.props.starred }, { 'fa-star-o': !this.props.starred })} />
          </button>
          <button className={`btn btn-default ${styles.btnAction}`} onClick={() => this.props.deleteFriend(this.props.id)}>
            <i className="fa fa-trash" />
          </button>
        </div>
      </li>
    );
  }

}
