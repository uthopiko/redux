import * as types from '../constants/ActionTypes';

export function addFriend(name) {
  return {
    type: types.ADD_FRIEND,
    name
  };
}

export function deleteFriend(id) {
  return {
    type: types.DELETE_FRIEND,
    id
  };
}

export function starFriend(id) {
  return {
    type: types.STAR_FRIEND,
    id
  };
}

export function editFriend(id) {
  return {
    type: types.EDIT_FRIEND,
    id
  };
}

export function saveName(id, value) {
  return {
    type: types.SAVE_NAME,
    id, 
    value
  };
}
